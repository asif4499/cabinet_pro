<?php

namespace Asif\Classes;

use PDO, PDOException;
//class User
//{
//    public $full_name = 'asif';
//    public $date_of_birth = null;
//    public $gender = null;
//    public $email = null;
//    public $location = null;
//    public $number = null;
//    public $password1 = null;
//    public $password2 = null;
//    public $pdf = null;
//    public $image = null;
//
//    public function setdata(){
//
//    }
//    public function store(){
//
//    }
//}
//$users = new User();
//$users->setdata();

class User
{
    public $name;
    public $dob;
    public $gender;
    public $email;
    public $division;
    public $zilla;
    public $phone;
    public $pass1;
    public $pass2;
    public $pdf;
    public $img;
    private $servername = "localhost";
    private $username = "root";
    private $password = "1234";
    private $conn;

    public function __construct()
    {
        session_start();
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=cabinet", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `user_data`";
        $stmt = $this->conn->query($sql);
        $data = $stmt->fetchAll();
        return $data;
    }

    public function setData($data)
    {
        if (array_key_exists('full_name', $data) && !empty($data['full_name'])) {
            $this->name = $data['full_name'];
        }

        if (array_key_exists('date_of_birth', $data) && !empty($data['date_of_birth'])) {
            $this->dob = $data['date_of_birth'];
        }

        if (array_key_exists('gender', $data) && !empty($data['gender'])) {
            $this->gender = $data['gender'];
        }

        if (array_key_exists('email', $data) && !empty($data['email'])) {
            $this->email = $data['email'];
        }

        if (array_key_exists('state', $data) && !empty($data['state'])) {
            $this->division = $data['state'];
        }

        if (array_key_exists('zilla', $data) && !empty($data['zilla'])) {
            $this->zilla = $data['zilla'];
        }

        if (array_key_exists('number', $data) && !empty($data['number'])) {
            $this->phone = $data['number'];
        }

        if (array_key_exists('password1', $data) && !empty($data['password1'])) {
            $this->pass1 = $data['password1'];
        }

        if (array_key_exists('image', $data) && !empty($data['image'])) {
            $this->img = $data['image'];
        }

        if (array_key_exists('pdf', $data) && !empty($data['pdf'])) {
            $this->pdf = $data['pdf'];
        }
    }

//    public function store()
//    {
////        $sql = "INSERT INTO books(title) values('$this->title')";
//        $sql = "INSERT INTO `user_data`(`name`, `date_of_birth`, `gender`, `email`, `state`, `zilla`, `phone`,
////                `password`, `pdf`, `image`) VALUES ($this->name, $this->dob, $this->gender, $this->email, $this->division, $this->zilla,
////                $this->phone,$this->pass1,$this->img,$this->pdf)";
//        $this->conn->exec($sql);
//        $_SESSION['message'] = "New record created successfully";
//        header('location: index.php');
//    }

    public function store()
    {
        try{
            $sql = "INSERT INTO `user_data`(`name`, `date_of_birth`, `gender`, `email`, `state`, `zilla`, `phone`, `password`, `pdf`, `image`) VALUES (?,?,?,?,?,?,?,?,?,?)";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $this->name);
            $stmt->bindParam(2, $this->dob);
            $stmt->bindParam(3, $this->gender);
            $stmt->bindParam(4, $this->email);
            $stmt->bindParam(5, $this->division);
            $stmt->bindParam(6, $this->zilla);
            $stmt->bindParam(7, $this->phone);
            $stmt->bindParam(8, $this->pass1);
            $stmt->bindParam(9, $this->pdf);
            $stmt->bindParam(10, $this->img);
            $stmt->execute();
        }
        catch (PDOException $exception){
            echo $exception->getMessage();
        }
    }


    public function show()
    {
        $sql = "SELECT * FROM `books` WHERE id=$this->id";
        $stmt = $this->conn->query($sql);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function update()
    {
        $sql = "UPDATE books SET title='$this->title' WHERE id=$this->id";
        $this->conn->exec($sql);
        $_SESSION['message'] = "New record Updated successfully";
        header('location: index.php');
    }

    public function destroy()
    {
        $sql = "DELETE FROM books WHERE id=$this->id";
        $this->conn->exec($sql);
        $_SESSION['message'] = "Deleted successfully";
        header('location: index.php');
    }
}