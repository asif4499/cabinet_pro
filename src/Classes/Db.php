<?php
class Database
{
    public $conn;
    public $servername = "localhost", $username = "root",$password = "1234";
    
    function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=c-pro", $this->username, $this->password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully"; 
            }
        catch(PDOException $e)
            {
            echo "Connection failed: " . $e->getMessage();
            } 
    }
}

?>