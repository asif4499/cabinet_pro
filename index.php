<?php include('assest/partials/header.php');?>

<div class="container">
    <div class="row">
        <div class="pro-pic col-md-4">
            <img class="img-fluid" src="../picture/1.jpg" alt="">
        </div>
        <div class="col-md-8">
            <button type="button" class="btn btn-outline-success btn-lg btn-block">Download Profile as pdf</button>
        </div>
    </div>
    <br>
    <table class="table table-borderless">
        <tbody>
        <tr>
            <th scope="row">Name</th>
            <td>:</td>
            <td>All Imam Asif (Ridoy)</td>
        </tr>
        <tr>
            <th scope="row">Date of Birth</th>
            <td>:</td>
            <td>17/12/1995</td>
        </tr>
        <tr>
            <th scope="row">Gender</th>
            <td>:</td>
            <td>Male</td>
        </tr>
        <tr>
            <th scope="row">Email</th>
            <td>:</td>
            <td>all.imam.asif@gmail.com</td>
        </tr>
        <tr>
            <th scope="row">Address</th>
            <td>:</td>
            <td>175 Middle Isdair, Fatullah, Narayanganj</td>
        </tr>
        <tr>
            <th scope="row">Phone</th>
            <td>:</td>
            <td>+8801515627209</td>
        </tr>
        <tr>
            <th scope="row">CV</th>
            <td>:</td>
            <td><a href="">Download</a></td>
        </tr>
        </tbody>
    </table>
</div>

<?php include('assest/partials/footer.php');?>