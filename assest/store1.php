<?php include('../partials/header.php');?>
<div class="container">
    <table class="table table-bordered t-c">
        <thead>
        <tr>
            <th scope="col">SL</th>
            <th scope="col">Image</th>
            <th scope="col">Name</th>
            <th scope="col">Gender</th>
            <th scope="col">Phone</th>
            <th scope="col">Email</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <th><img class="p-img" src="../picture/1.jpg" alt=""></th>
            <td class="align-middle">All Imam Asif</td>
            <td class="align-middle">Male</td>
            <td class="align-middle">+8801515627209</td>
            <td class="align-middle">all.imam.asif@gmail.com</td>
            <td class="align-middle"><button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-user-circle"></i></button>
                <button type="button" class="btn btn-outline-info"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button></td>

        </tr>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-outline-success btn-lg btn-block">Download as pdf</button>
            </div>
            <div class="modal-body">
                <!-- Image -->
                <div class="text-center">
                    <img src="../picture/1.jpg" alt="" style="height: 15em;">
                </div>
                <!-- Info -->
                    <table class="table table-borderless t-details">
                        <tbody>
                        <tr>
                            <th scope="row">Name</th>
                            <td>:</td>
                            <td>All Imam Asif (Ridoy)</td>
                        </tr>
                        <tr>
                            <th scope="row">Date of Birth</th>
                            <td>:</td>
                            <td>17/12/1994</td>
                        </tr>
                        <tr>
                            <th scope="row">Gender</th>
                            <td>:</td>
                            <td>Male</td>
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>:</td>
                            <td>all.imam.asif@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">Address</th>
                            <td>:</td>
                            <td>175no Middle Isdair, Fatullah, Narayanganj</td>
                        </tr>
                        <tr>
                            <th scope="row">Phone</th>
                            <td>:</td>
                            <td>+8801515627209</td>
                        </tr>
                        <tr>
                            <th scope="row">CV</th>
                            <td>:</td>
                            <td><button type="button" class="btn btn-link">Download</button></td>
                        </tr>
                        </tbody>
                    </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php include('../partials/footer.php');?>
