<!doctype html>
<html lang="en">
<head>
    <link rel="icon" href="../picture/icons8-checked-480.png" type="image/gif" sizes="16x16">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <style>
        a:hover {
            text-decoration: none;
        }
        .card-body p{
            font-size: 1.4em;
        }
        .card-body h5{
            margin-top: 5PX;
        }
        .t-c th{
            text-align: center;
            vertical-align: middle;
        }
        .t-c tr{
            text-align: center;
        }
        .p-img{
            height: 6em;
        }
        .img-frame{
            height: 15em !important;
        }
        .t-details th,
        .t-details tr{
            text-align: left;
        }
        .panel-title{
            text-align: center;
        }
        .panel-heading h4{
            color: red;
            text-align: center;
        }
        .middle{
            position: relative;
            top: 50%;

        }
    </style>
    <title>Registration</title>
</head>
<body>