<?php include('../partials/no-nav.php');?>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default middle">
                <div class="panel-heading">
                    <h4 text-justify style="color: green !important;">Login via site</h4>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="yourmail@example.com" name="email" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                                </label>
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                        </fieldset>
                    </form>
                    <hr/>
                    <center><h4>OR</h4></center>

                    <a href="assest/pages/reg.php"><input class="btn btn-lg btn-primary btn-block" type="submit" value="Sing Up"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../partials/footer.php');?>