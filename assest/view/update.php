<?php include('../partials/no-nav.php');?>
<div class="container">
    <div class="card bg-light">
        <div class="d-flex justify-content-center">
            <article class="card-body" style="max-width: 50%;">
                <h4 class="card-title mt-3 text-center">Update Account</h4>
                <form>
                    <h5>Name</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                        </div>
                        <input name="" class="form-control" placeholder="Full name" type="text">
                    </div>
                    <!-- Name -->

                    <h5>Date of birth</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fas fa-calendar-alt"></i> </span>
                        </div>
                        <input name="" class="form-control" placeholder="Full name" type="date">
                    </div>
                    <!--Date of birth-->
                    <h5>Gender</h5>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Male
                        </label>
                        <input class="form-check-input-1" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label-1" for="exampleRadios2">
                            Female
                        </label>
                    </div>


                    <h5>Email</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="" class="form-control" placeholder="Email address" type="email">
                    </div>
                    <!-- Email -->

                    <h5>District</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-building"></i> </span>
                        </div>
                        <select class="form-control">
                            <option selected=""> Select District</option>
                            <option>Designer</option>
                            <option>Manager</option>
                            <option>Accaunting</option>
                        </select>
                        <select class="form-control">
                            <option selected="">-- select --</option>
                            <option>Designer</option>
                            <option>Manager</option>
                            <option>Accaunting</option>
                        </select>
                    </div>
                    <!-- District -->

                    <h5>Phone</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        </div>
                        <select class="custom-select" style="max-width: 120px;">
                            <option selected="">+880</option>
                            <option value="1">+972</option>
                            <option value="2">+198</option>
                            <option value="3">+701</option>
                        </select>
                        <input name="" class="form-control" placeholder="Phone number" type="number">
                    </div>
                    <!-- Phone -->

                    <h5>Password</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input class="form-control" placeholder="Create password" type="password">
                    </div> <!-- form-group// -->

                    <h5>Repeat Password</h5>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input class="form-control" placeholder="Repeat password" type="password">
                    </div> <!-- form-group// -->

                    <div class="form-group">
                        <label for="exampleFormControlFile1"><h5>Upload pdf</h5></label>
                        <input accept="application/pdf" type="file" class="form-control-file" id="exampleFormControlFile1">
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlFile2"><h5>Upload image</h5></label>
                        <input accept="image/jpeg" type="file" class="form-control-file" id="exampleFormControlFile2">
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="button" class="btn btn-success">Update</button>
                        &nbsp
                        &nbsp
                        <button type="button" class="btn btn-danger"><a href="store.php">Back</a></button>
                    </div> <!-- form-group// -->
                </form>
            </article>
        </div>
    </div> <!-- card.// -->

</div>

<?php include('../partials/footer.php');?>